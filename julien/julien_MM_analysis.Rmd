---
title: "MickeyMouse Analysis"
author: "Julien"
date: "2024-06-04"
output: html_document
---

# Alzheimer's in mice - Preliminary Analysis

## Step 1: Load in libraries and data

```{r}
library(Seurat)
library(dplyr)

data = Read10X_h5("../data/VisiumFFPE_Mouse_Brain_Alzheimers_AppNote_filtered_feature_bc_matrix.h5")
sobj = CreateSeuratObject(counts = data,
                          project = "Mouse_AD",
                          min.cells = 3, 
                          min.features = 200)
sobj2 = CreateSeuratObject(counts = data,
                          project = "Mouse_AD",
                          min.cells = 3, 
                          min.features = 200)

# Subsetting Seurat object
sobj_treat1_2 <- subset(x = sobj2, subset = Treatment.id == c("1", "2"))

sobj_treat7_8 <- subset(x = sobj2, subset = Treatment.id == c("7", "8"))


treatment_info = read.csv("../data/VisiumFFPE_Mouse_Brain_Alzheimers_AppNote_aggregation.csv")
# Getting rid of columns with file paths
treatment_info = treatment_info[, -c(2, 3, 4)]
# Adding numbers 1 through 12 to match treatments with barcodes
treatment_info$Treatment.id = 1:nrow(treatment_info)

sobj
```

Let us see the structure of the Seurat object

```{r}
sobj@assays
```

```{r}
# raw counts of cells
sobj@assays$RNA@layers$counts[1:10, 1:10]

# sparse Matrix results because not every cell type expresses every gene
```

```{r}
# see cell barcode of the data
sobj@assays$RNA@cells@.Data %>% tail(50)
```

```{r}
# see the features (gene names)
sobj@assays$RNA@features@.Data %>% head(10)
```

```{r}
sobj@meta.data %>% head(20)

# gives overview of cell types, counts of these cells and the number of expressed genes
```

## Step 2: Preprocess and quality control

Mitochondrial DNA (mtDNA)

Important to filter out cells with high mt expression, because the mt of dying cells can give an impression of higher expression levels because the DNA leaks in the cells.

--\> No MT data, trimmed

Adding treatment info to meta data of the Seurat object.

```{r}
# sobj[["percent.T1"]] = PercentageFeatureSet(sobj, pattern = "-1$")

# Adding empty columns for treatment information later
for (column_name in colnames(treatment_info)) {
    sobj[[column_name]] <- NA
}
# Extract the barcode suffixes from the Seurat object:
barcodes <- colnames(sobj2@assays$RNA)
suffixes <- sub(".*-(\\d+)$", "\\1", barcodes)

# Create a mapping between the suffixes and the treatment information:
# Ensure Treatment.id is a character for proper matching
treatment_info$Treatment.id <- as.character(treatment_info$Treatment.id)

# Create a named list for easy lookup based on Treatment.id
treatment_list <- split(treatment_info, treatment_info$Treatment.id)

# Populate the metadata in the Seurat object:
# Initialize a data frame to store the treatment information for each cell
treatment_data <- data.frame(matrix(nrow = length(suffixes), ncol = ncol(treatment_info)))
colnames(treatment_data) <- colnames(treatment_info)

# Fill the treatment_data based on the suffixes
for (i in 1:length(suffixes)) {
    suffix <- suffixes[i]
    if (suffix %in% names(treatment_list)) {
        treatment_data[i, ] <- treatment_list[[suffix]]
    } else {
        treatment_data[i, ] <- NA
    }
}

# Add the treatment data to the Seurat object's metadata
for (col in colnames(treatment_data)) {
    sobj2[[col]] <- treatment_data[, col]
}

# View updated metadata to verify
tail(sobj2@meta.data)

```

```{r}
# Visualize the results
VlnPlot(sobj_treat1_2,
        features = c("nFeature_RNA", "nCount_RNA"),
        ncol = 3)

VlnPlot(sobj_treat7_8,
        features = c("nFeature_RNA", "nCount_RNA"),
        ncol = 3)
# No MT DNA
```

```{r}
# filter outliers from expression data
sobj = subset(
  sobj,
  subset = nFeature_RNA > 2500 & nFeature_RNA < 8500
)
sobj_treat1_2 = subset(
  sobj_treat1_2,
  subset = nFeature_RNA > 3000 & nFeature_RNA < 8500
)
sobj_treat7_8 = subset(
  sobj_treat7_8,
  subset = nFeature_RNA > 3000 & nFeature_RNA < 8500
)

```

## Step 3: Normalize the data

We use log normalization to normalize our data.

```{r}
sobj = NormalizeData(sobj, normalization.method = "LogNormalize")
sobj@assays$RNA

sobj_treat1_2 = NormalizeData(sobj_treat1_2, normalization.method = "LogNormalize")

sobj_treat7_8 = NormalizeData(sobj_treat7_8, normalization.method = "LogNormalize")
```

```{r}
sobj@assays$RNA@layers$data[1:5, 1:3]
```

## Step 4: Feature extraction

```{r}
# Find top 1000 features (genes)
sobj = FindVariableFeatures(sobj, nfeatures = 1000)
sobj_treat1_2 = FindVariableFeatures(sobj_treat1_2, nfeatures = 1000)
sobj_treat7_8 = FindVariableFeatures(sobj_treat7_8, nfeatures = 1000)
# Identify the 10 most highly variable genes
top10 = VariableFeatures(sobj) %>% head(10)
top10_sobj_treat1_2 = VariableFeatures(sobj_treat1_2) %>% head(10)
top10_sobj_treat7_8 = VariableFeatures(sobj_treat7_8) %>% head(10)
# Plot variable features with and without labels
plot = VariableFeaturePlot(sobj)
plot = LabelPoints(plot = plot, points = top10, repel = TRUE)
plot

plot = VariableFeaturePlot(sobj_treat1_2)
plot = LabelPoints(plot = plot, points = top10_sobj_treat1_2, repel = TRUE)
plot

plot = VariableFeaturePlot(sobj_treat7_8)
plot = LabelPoints(plot = plot, points = top10_sobj_treat7_8, repel = TRUE)
plot

# Standardized variance tells us how different the genes are across cells
```

## Step 5: Dimensional Reduction

Here we first scale the data and then perform principal component analysis (PCA).

```{r}
# Create uniform scaling for data points
sobj = ScaleData(sobj)
sobj

sobj_treat1_2 = ScaleData(sobj_treat1_2)

sobj_treat7_8 = ScaleData(sobj_treat7_8)
```

```{r}
# visualize pca results in elbow plot, through dimensional reduction
sobj = RunPCA(sobj, features = VariableFeatures(object = sobj))


sobj_treat1_2 = RunPCA(sobj_treat1_2, features = VariableFeatures(object = sobj_treat1_2))


sobj_treat7_8 = RunPCA(sobj_treat7_8, features = VariableFeatures(object = sobj_treat7_8))

# The principal component in a principal component analysis (PCA) is a new, uncorrelated variable derived from the original variables that captures the maximum possible variance in the data.

# Tries to find the optimal number of features (ie. Benign, Malignant, Specific gene expression levels) to describe the maximum possible variance.
ElbowPlot(sobj)

ElbowPlot(sobj_treat1_2)

ElbowPlot(sobj_treat7_8)
```

## Step 6: Clustering the cells

Here we used UMAP (Uniform Manifold Approximation and Projection) to cluster the cells.

Also used as an addendum for previous step for non-linear data.

```{r}
# dimensions dependant on output of previous step
# range dependant on highest non-stable PC value

# SNN: Shared Nearest Neighbor
sobj = FindNeighbors(sobj, dims = 1:18) 

sobj_treat1_2 = FindNeighbors(sobj_treat1_2, dims = 1:10) 

sobj_treat7_8 = FindNeighbors(sobj_treat7_8, dims = 1:18) 

# Default resolution 1, higher value results in higher res. (more cell types)
sobj = FindClusters(sobj, resolution = 4) 
sobj_treat1_2 = FindClusters(sobj_treat1_2, resolution = 1)
sobj_treat7_8 = FindClusters(sobj_treat7_8, resolution = 4)
# UMAP
sobj = RunUMAP(sobj, dims = 1:18)
DimPlot(sobj, reduction = "umap")

sobj_treat1_2 = RunUMAP(sobj_treat1_2, dims = 1:10)
DimPlot(sobj_treat1_2, reduction = "umap")

sobj_treat7_8 = RunUMAP(sobj_treat7_8, dims = 1:18)
DimPlot(sobj_treat7_8, reduction = "umap")
```

## Step 7: Save your Seurat object as a rds file

A review of your Seurat object. Now you have finished the first part of standard workflow of scRNAseq then you can save your Seurat object for future usage.

```{r}
saveRDS(sobj, file = "./mickeymouse_seurat_day1.rds")

saveRDS(sobj_treat1_2, file = "../data/seurat_treat1_2.rds")

saveRDS(sobj_treat7_8, file = "../data/seurat_treat7_8.rds")

# read Seurat rds object
sobj = readRDS("../data/mickeymouse_seurat_day1.rds")


sobj_treat1_2 = readRDS("../data/seurat_treat1_2.rds.rds")


sobj_treat7_8 = readRDS("../data/seurat_treat7_8.rds.rds")
```

# Analysis Day 2

Trying to narrow down clusters with automatic cell annotation methods from celldex and SingleR.

```{r}
# Load libraries for cell annotation
library(SingleR)
library(Seurat)
library(celldex)
library(dplyr)
library(pheatmap)
```

```{r}
# Find top markers for all clusters
# Grouping neccessary to get top 3 for every cluster
sobj.markers %>%
  group_by(cluster) %>%
  dplyr::filter(avg_log2FC > 1 & p_val < 0.05 & pct.1 > 0.9) %>%
  slice_head(n = 3) -> top3

sobj_treat1_2.markers %>%
  group_by(cluster) %>%
  dplyr::filter(avg_log2FC > 1 & p_val < 0.05 & pct.1 > 0.9) %>%
  slice_head(n = 3) -> top3_treat1_2

sobj_treat7_8.markers %>%
  group_by(cluster) %>%
  dplyr::filter(avg_log2FC > 1 & p_val < 0.05 & pct.1 > 0.9) %>%
  slice_head(n = 3) -> top3_treat7_8
```

## Step X: Automatic cell annotation with SingleR

## Prepare inputs for running automatic cell type annotation

We need to extract the normalization data using GetAssayData()

```{r}
sobj_treat1_2_count = GetAssayData(sobj_treat1_2, layer = "data") 
str(sobj_treat1_2_count)

sobj_treat7_8_count = GetAssayData(sobj_treat7_8, layer = "data") 
str(sobj_treat7_8_count)
```

Get the Blueprint and Encode reference data from celldex package:

```{r}
ref_treat1_2 = celldex::MouseRNAseqData() 
ref_treat1_2

ref_treat7_8 = celldex::MouseRNAseqData() 
ref_treat7_8
```

## Run SingleR to label cell types

```{r}
pred_treat1_2 = SingleR(test = sobj_treat1_2_count, ref = ref_treat1_2, labels = ref_treat1_2$label.main)

pred_treat7_8 = SingleR(test = sobj_treat7_8_count, ref = ref_treat7_8, labels = ref_treat7_8$label.main)
```

Next, we put the prediction results back to our Seurat object for visualization

```{r}
sobj_treat1_2$labels = pred_treat1_2[match(rownames(sobj_treat1_2@meta.data), rownames(pred_treat1_2)), 'labels'] 
sobj_treat1_2@meta.data %>% head()

sobj_treat7_8$labels = pred_treat7_8[match(rownames(sobj_treat7_8@meta.data), rownames(pred_treat7_8)), 'labels'] 
sobj_treat7_8@meta.data %>% head()
```

## Visualize the data

```{r}
umap_pred_treat1_2 = DimPlot(   
  sobj_treat1_2, reduction = 'umap', group.by = 'labels', label = TRUE, repel = TRUE)
umap_pred_treat1_2

umap_pred_treat7_8 = DimPlot(   
  sobj_treat7_8, reduction = 'umap', group.by = 'labels', label = TRUE, repel = TRUE)
umap_pred_treat7_8
```

Plot the SingleR assignment score across all cell-label combinations

```{r}
plotScoreHeatmap(pred_treat1_2)

plotScoreHeatmap(pred_treat7_8)
```

## Step X: Find cell markers

```{r}

# Find all markers
sobj_treat1.markers = FindAllMarkers(sobj_treat1, only.pos = TRUE)

sobj_treat1_2.markers = FindAllMarkers(sobj_treat1_2, only.pos = TRUE)
sobj_treat1_2.markers %>%
  head(100)

sobj_treat7_8.markers = FindAllMarkers(sobj_treat7_8, only.pos = TRUE)
head(sobj_treat7_8.markers)

# pct 1 = proportion of cells from chosen cluster which express the marker
# pct 2 = proportion of cells from all other clusters which express this marker
# avglog = +- indicated high and low expression levels
# --> finding a gene that is expressed in all clusters aside from target one can also serve as a marker

```

## Marker based annotation with Cellmarker

```{r}
# Read in CellMarker excel file
library(readxl)
cellmarkers = read_xlsx("../data/Cell_marker_All.xlsx")


cellmarker_filtered = dplyr::filter(cellmarkers, species == 'Mouse', cancer_type == 'Normal', tissue_class == 'Brain')
rm(cellmarkers)

#CMF_subset = cellmarker_filtered[, c(7:9)]

# Combine CellMarker with our markers sobj.markers.
merg_cellm_treat1_2 = sobj_treat1_2.markers %>%
  filter(p_val_adj < 0.05 & avg_log2FC > 1 & pct.1 > 0.8 & pct.2 < 0.4) %>%
  left_join(cellmarker_filtered[,c(3,7,8,9)], join_by(gene==marker), relationship = "many-to-many")

merg_cellm_treat1_2 %>% filter(cell_name == "Oligodendrocyte")

merg_cellm_treat7_8 = sobj_treat7_8.markers %>%
  filter(p_val_adj < 0.05 & avg_log2FC > 1 & pct.1 > 0.8 & pct.2 < 0.5) %>%
  left_join(cellmarker_filtered[,c(3,7,8,9)], join_by(gene==marker), relationship = "many-to-many")

merg_cellm_treat7_8 %>% head(100)
```
