# This is Lea´s folder

In my folder you can find my RMarkDown document as well as the figures that were created during the project.

Here I give you an outline from my analysis of comparing old transgenic mice to old wildtype mice.

## Marker based annotation of cells

### Find the markers

1.  Find all the markers with the FindAllMarkers() command.

2.  Filter the marker list from CellMarker 2.0 for mouse cells without cancer indication and from the brain.

3.  Merge all marker with the marker list from CellMarker with only significantly different gene expression of the markers Over 85% of the target cells should express the marker and under 50% of the reference cells should not express the marker. \
    Example: In cluster 2 more than 85% express maker A and less than 50% in the other clusters express marker A.

### Prepare a heatmap for the counts of marker in each cluster

![Fig. 1: Heatmap of markers for each cluster.](figures/marker_heatmap.png)

### Check the markers in a cluster

In cluster 19 three makers only for that cluster are shown. In marker_merge markers can be checked and a Feature Plot can be created.

![Fig. 2: Markers for identifying cell type of cluster 19.](figures/feature_plot.png)

### Create a subset of the single cluster and wildtype/transgenic and find the marker

1.  Get the data of the ID and cluster of the Seurat Object.

2.  Subset two data frames with the two transgenic old mice and two old wildtype mice and one cluster (here I chose cluster 19 as it had 3 similar marker).

3.  Compare the expression of transgenic mice to wildtype mice with FindMarkers() function.

4.  Filter the results for significance (p\<0.05).

5.  Check the table of the results (table with 1.703 entries, Fold Change, p-values and pct´s can be checked).

### GO Analysis

1.  Extract the Fold Change of the significant results and sort it decreasingly.

2.  Run a gseGO() analysis with the sorted Fold Change, the specific organism database (*Mus Musculus*).

3.  Create a dotplot where the GO-categories and their significance are shown.

![Fig. 3: Dotplot Gene Set Enrichment Analysis for Gene Ontology of Cluster 19.](figures/dotplot_gse.png)

### Annotate the cell type in the UMAP plot

Rename the identification of cluster 19 with "Ependymal cells".

![Fig. 4: Annotated UMAP plot.](figures/umap_wt_tg.png)
